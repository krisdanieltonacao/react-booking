const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	type:{
		type: String,
		required: [true, "Product Type is required"]
	},
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description01: {
		type: String,
		required: [true, "Description is required"]
	},
	description02: {
		type: String,
		required: [true, "Description is required"]
	},
	stocks:{
		type: Number,
		required: [true, "Slot is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	imageMain: {type: String},
	video: {type: String},
	image002: {type: String},
	image003: {type: String},
	image004: {type: String},
	image005: {type: String},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn :{
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userId:{
				type: String,
				required: [true, "UserId is Required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);
