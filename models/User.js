const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password01: {
		type: String,
		required: [true, "Passwword is required"]
	},
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo :{
		type: String,
		required: [true, "Mobile number is required"]
	},
	birthDate : {type: Date},
	address:  {type: String},
	eWallet: {
		company: {type: String},
		accountNo:  {type: String},
	},
	cardPayment:{
		issuer: {type: String},
		cardNo:  {type: String},
		expDate: {type: String},
		cvv:  {type: String}
	},
	isAdmin :{
		type: Boolean,
		default: false
	},
	createdOn:{
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("User", userSchema);