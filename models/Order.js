const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
		userId: {type: String},
        lastName: {type: String},
        firstName: {type: String},
        address: {type: String},
		mobileNo: {type: String},
		productId: {type: String},
		name: {type: String},
		image005: {type: String},
		price: {type: Number},
		quantity: {type: Number},
        subTotal: {type: Number},
        status: {
			type: String,
			default: "Pending" // "pending / paid / shipped"
		},
		createdOn :{
			type: Date,
			default: new Date()
		}
})
module.exports = mongoose.model("Order", orderSchema);
