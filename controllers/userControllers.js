const User = require("../models/User");
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// OKAY
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	});
}
// OKAY
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email: reqBody.email,
		password01: bcrypt.hashSync(reqBody.password01, 10),
		isAdmin: reqBody.isAdmin,
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		birthDate: reqBody.birthDate,
		address: reqBody.address,
		eWallet:{
				company: reqBody.company,
				accountNo: reqBody.accountNo,
		},
		cardPayment:{
			issuer: reqBody.issuer,
			cardNo: reqBody.cardNo,
			expDate: reqBody.expDate,
			cvv: reqBody.cvv
		}
	})
	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
// OKAY
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password01, result.password01);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}

	})
}
// OKAY
module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result =>{
		result.password ="";
		return result;
	})
}

module.exports.getCart = (data) =>{
		return Order.find({userId: data.userId}).then(result =>result)};
		
module.exports.purchase = async (data) =>{
			let newOrder 
				let isOrderUpdated = await Order.findById(data.userId).then(orderResult =>{
							
							newOrder = new Order({
								userId: data.userId,
								lastName: data.lastName,
								firstName: data.firstName,
								address: data.address,
								mobileNo: data.mobileNo,
								productId: data.productId,
								name: "dummy",
								image005: "dummy",
								price: 0,
								quantity: data.quantity,
								subTotal: 0
							})
							return newOrder.save().then((order, error) =>{
								if(error){
									return false;
									}
								else{
									return true;
								}	
							})
					})
				let isProductUpdated = await Product.findById(data.productId).then(product =>{
					product.stocks = product.stocks - data.quantity;
					return product.save().then((stocks, error) =>{
						if(error){
							return false;
						}
						else{
							return true;
						}
					})
				})
			
				let isPriceUpdated = await Product.findById(newOrder.productId).then(priceResult =>{
					newOrder.name = priceResult.name;
					newOrder.image005 = priceResult.image005;
					newOrder.price = priceResult.price;
					newOrder.subTotal = priceResult.price * newOrder.quantity;
					return newOrder.save().then((order, error)=>{
						if(error){
							return false;
						}
						else{
							return true;
						}
					})
				})
				
				if(isOrderUpdated && isProductUpdated && isProductUpdated){
					return true;
					}
					else{
						return false;
					}
			
			}

module.exports.getAllOrders = () =>{
	return Order.find({}).then(result => result);
}

module.exports.checkOut = (orderId, reqBody) =>{
	let updateActiveField = {
		status : reqBody.status
	}
	return Order.findByIdAndUpdate(orderId, updateActiveField).then((isActive, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

module.exports.updateUserDetails = (userData, reqBody) =>{
	let updatedDetails = {
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		birthDate: reqBody.birthDate,
		address: reqBody.address,
		eWallet:{
				company: reqBody.company,
				accountNo: reqBody.accountNo,
		},
		cardPayment:{
			issuer: reqBody.issuer,
			cardNo: reqBody.cardNo,
			expDate: reqBody.expDate,
			cvv: reqBody.cvv
		}
	}

	return User.findByIdAndUpdate(userData.userId, updatedDetails).then((userUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}