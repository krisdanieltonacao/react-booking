const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		type: reqBody.type,
		name: reqBody.name,
		description01: reqBody.description01,
		description02: reqBody.description02,
		stocks: reqBody.stocks,
		price: reqBody.price,
		imageMain: reqBody.imageMain,
		video: reqBody.video,
		image002: reqBody.image002,
		image003: reqBody.image003,
		image004: reqBody.image004,
		image005: reqBody.image005
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result);
}

module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result => result);
}

module.exports.getOneProduct = (productId) =>{
	return Product.findById(productId).then(result=>result);
}

module.exports.displayConsoleTitles = (machine) =>{
	
	return Product.find({description01:`${machine}`}).then(result => result);
}

module.exports.updateProduct = (productId, reqBody) =>{
	let updatedProduct = {
		type: reqBody.type,
		name: reqBody.name,
		description01: reqBody.description01,
		description02: reqBody.description02,
		stocks: reqBody.stocks,
		price: reqBody.price,
		imageMain: reqBody.imageMain,
		video: reqBody.video,
		image002: reqBody.image002,
		image003: reqBody.image003,
		image004: reqBody.image004,
		image005: reqBody.image005
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (productId, reqBody) =>{
	let updateActiveField = {
		isActive : reqBody.isActive
	}

	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

module.exports.changeStatus = (productId, reqBody) =>{
	let updateActiveField = {
		status : reqBody.status
	}

	return Order.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

