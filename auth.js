const jwt = require("jsonwebtoken");
const secret =  "crushAkoNgCrushKo";

module.exports.createAccessToken = (user) =>{
	// When the user logs in, a token will be created with the user's information.
	// This will be used for the token payload
	const data = {
		id: user._id,
		email: user.email,
		firstName: user.firstName,
		lastName: user.lastName,
		mobileNo: user.mobileNo,
		address: user.address,
		isAdmin: user.isAdmin
	}
	console.log(data);
	return jwt.sign(data, secret, {});
}


module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	console.log(typeof token);

	if(typeof token !== "undefined"){
		
		token = token.slice(7, token.length);
		console.log(token);

		
		return jwt.verify(token, secret, (err, data)=>{
		
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{
		
				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}
}


module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{

			if(err){
				return null;
			}
			else{

				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}

}