const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register",(req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res)=>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})


router.get("/cart", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	userControllers.getCart({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})



router.post("/purchase/:productId", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
        lastName: userData.lastName,
        firstName: userData.firstName,
        address: userData.address,
		mobileNo: userData.mobileNo,
        productId: req.params.productId,
		name: "",
        image005: "",
		price: 0,
		quantity: 1,
        subTotal: 0
	}
	if(userData.isAdmin){
		res.status(404).send("You're not allowed to access this page!");
	}
	else{
		userControllers.purchase(data).then(resultFromController => res.send(resultFromController));
	}
})

router.get("/allOrders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userControllers.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.status(404).send("You're not allowed to access this page!");
	}
})

router.patch("/:orderId/cartout", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	userControllers.checkOut(req.params.orderId, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/edit", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	userControllers.updateUserDetails(userData, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;