const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
 
router.post("/", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission on this page!");
	}
	
})

router.get("/all", auth.verify, (req, res) =>{
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req,res)=>{
	productControllers.getOneProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

router.get("/all/:description01", (req, res) =>{
	productControllers.displayConsoleTitles(req.params.description01).then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", auth.verify, (req, res)=>{
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

router.patch("/:productId/archive", auth.verify, (req, res) =>{
	productControllers.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

router.patch("/:productId/status", auth.verify, (req, res) =>{
	productControllers.changeStatus(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;